.mfs all:
all: mfs

mfs: mfs.c
	gcc mfs.c -pthread -o mfs

.mfs clean:
clean:
	-rm -rf *.o *.exe mfs

