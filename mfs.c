/*
 * MFS.c
 *
 * Author: romil
 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>

// Struct representing the flows
typedef struct {
	int number;
	int arrival_time;
	int transmission_time;
	int priority;
	struct timeval start_time;
} Flow;

// Mutexes for transmission and queue
pthread_mutex_t trans_start, queueMutex = PTHREAD_MUTEX_INITIALIZER;

// Convar for transmission
pthread_cond_t transSig = PTHREAD_COND_INITIALIZER;


// Globals for current flow transmitting and queue
int currentflow = 0;
Flow *queue;

int findLines(char *);
void parseFlow(Flow *, char *, int);
void *threadInit();
void addToQueueAfter(Flow *);
void sortQueue();
bool checkFlowInQueue(Flow *);
void requestTransmit(Flow *);
void transmit(Flow *);

int main(int argc, char *argv[]) {
	if(argc == 1) {
		printf("No filename specified. Usage: %s <filename>\n", argv[0]);
		exit(1);
	}

	// number of flows
	int numberOfLines;
	numberOfLines = findLines(argv[1]);

	Flow *flows = (Flow*) malloc(numberOfLines * sizeof(Flow));

	// Size of queue is 1 + number of flows
	queue = (Flow*) malloc(numberOfLines * sizeof(Flow) + 1);

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_t pid[numberOfLines];

	// Grab flow data from file
	parseFlow(flows, argv[1], numberOfLines);
	int i;
	for(i = 0; i < numberOfLines; i++) {
		// Create threads
		pthread_create(&pid[i], &attr, threadInit, &flows[i]);
	}
	for(i = 0; i < numberOfLines; i++) {
		// Wait for threads
		pthread_join(pid[i], NULL);
	}
	free(queue);
	return 0;
}

// Reads the first line of file to determine how many flows there are
int findLines(char *filename) {
	char read[128];
	FILE *file = fopen(filename, "r");
	if(file == 0) {
		printf("Unable to open file %s", filename);
		exit(1);
	}
	fgets(read, sizeof(read), file);
	fclose(file);
	return atoi(read);
}

// Parses each line of the file to get flow details
void parseFlow(Flow *flows, char *filename, int numberOfLines) {
	int i; char read[128];
	FILE *file = fopen(filename, "r");
	fgets(read, sizeof(read), file);
	for(i = 0; i < numberOfLines; i++) {
		int number, arrival, transmission, priority;
		fscanf(file, "%d", &number); fscanf(file, ":", NULL);
		fscanf(file, "%d", &arrival); fscanf(file, ",", NULL);
		fscanf(file, "%d", &transmission); fscanf(file, ",", NULL);
		fscanf(file, "%d", &priority); fscanf(file, ",", NULL);
		flows[i].number = number;
		flows[i].arrival_time = arrival * 100000;
		flows[i].transmission_time = transmission * 100000;
		flows[i].priority = priority;
	}
	fclose(file);
}

/*
 * Flow initializing thread, each flow will wait until it's arrival time
 * then it will request to be transmitted.
 */
void *threadInit(void *flow) {
	Flow *curFlow = (Flow*) flow;
	usleep(curFlow->arrival_time);
	gettimeofday(&curFlow->start_time, NULL);
	printf("Flow %2d arrives: arrival time (%.2f), transmission time (%.1f), priority (%2d). \n",
			curFlow->number, (double) curFlow->start_time.tv_sec, (float) curFlow->transmission_time,
			curFlow->priority);
	requestTransmit(curFlow);
}

// Adds a given flow to the end of the queue
void addToQueueAfter(Flow *flow) {
	int i;
	for(i = 0; i < sizeof(Flow)/sizeof(queue); i++) {
		if(queue[i+1].number == NULL) {
			queue[i+1] = *flow;
			return;
		}
	}
}

// Bubble sort used to reshuffle the queue
void sortQueue() {
	int i, j;
	for(i = 0; i < sizeof(Flow)/sizeof(queue) - 1; i++) {
		for(j = i+1; j < sizeof(Flow)/sizeof(queue); j++) {
			if(queue[j].number == NULL) {
				continue;
			}
			if(queue[j].priority > queue[i].priority) {
				Flow tmp = queue[i];
				queue[i] = queue[j];
				queue[j] = tmp;
			}
		}
	}
}

// Checks whether a flow is already in the queue to avoid duplicates
bool checkFlowInQueue(Flow *flow) {
	int i;
	for(i = 0; i < sizeof(Flow)/sizeof(queue); i++) {
		if(queue[i].number == flow->number) {
			return true;
		}
	}
	return false;
}

/*
 * Adds given flow to queue, sorts the queue then
 * tries to transmit the first element in the queue that's available.
 */
void requestTransmit(Flow *flow) {
	pthread_mutex_lock(&queueMutex);
	if(queue[0].number == NULL) { // if first flow then add right away
		queue[0] = *flow;
	} else if(checkFlowInQueue(flow) == false) { // if not first then add to end and sort
		addToQueueAfter(flow);
		sortQueue();
	}
	pthread_mutex_unlock(&queueMutex);

	while(true) {
		if(currentflow != 0) {
			// wait to receive signal that transmission is complete
			pthread_cond_wait(&transSig, &trans_start);
		}

		pthread_mutex_lock(&trans_start);
		pthread_mutex_lock(&queueMutex);
		int i,j;
		for(i = 0; i < sizeof(Flow)/sizeof(queue); i++) {
			// Find first available flow in queue to transmit
			if(queue[i].number != NULL) {
				for(j = i+1; j < sizeof(Flow)/sizeof(queue); j++) {
					// Notify about which flows are waiting for the current flow
					if(queue[j].number != NULL) {
						printf("Flow %2d waits for the finish of flow %2d. \n",
								queue[j].number, queue[i].number);
					}
				}
				// Transmit the flow
				transmit(&queue[i]);
				// Remove the flow
				queue[i].number = NULL;
			}
		}
		pthread_mutex_unlock(&queueMutex);
		pthread_mutex_unlock(&trans_start);
		break;
	}
}

/*
 * Transmits the given flow, signals when the transmission is complete
 */
void transmit(Flow *flow) {
	struct timeval starttime, finishtime;

	gettimeofday(&starttime, NULL);

	currentflow = flow->number;

	printf("Flow %2d starts its transmission at time %d. \n", flow->number, starttime.tv_sec);

	usleep(flow->transmission_time);
	gettimeofday(&finishtime, NULL);

	printf("Flow %2d finishes its transmission at time %d. \n", flow->number, finishtime.tv_sec);

	currentflow = 0;

	pthread_cond_signal(&transSig);
}
